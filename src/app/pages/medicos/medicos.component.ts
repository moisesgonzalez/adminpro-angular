import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MedicoService } from '../../services/service.index';
import { Medico } from '../../models/medico.model';

@Component({
  selector: 'app-medicos',
  templateUrl: './medicos.component.html',
  styles: []
})
export class MedicosComponent implements OnInit {

  desde: number = 0;
  medicos: Medico[] = [];

  cargando = false;
  realizoBusqueda = false;

  constructor(
    public http: HttpClient,
    public _medicoService: MedicoService
  ) { }

  ngOnInit() {
    this.cargarMedicos();
  }

  cargarMedicos() {
    this.cargando = true;
    this._medicoService.cargarMedicos(this.desde)
      .subscribe((medicos: any) => {
        this.cargando = false;
        this.medicos = medicos;
      });
  }

  cambiarDesde(valor: number) {
    let desde = this.desde + valor;

    if (desde >= this._medicoService.totalMedicos || desde < 0) {
      return;
    }

    this.desde += valor;
    this.cargarMedicos();
  }

  buscarMedicos(termino: string) {
    if (termino.length <= 0 && this.realizoBusqueda) {
      this.cargarMedicos();
      this.realizoBusqueda = false;
      return;
    }

    if (termino.length >= 3) {
      this.cargando = true;
      this.realizoBusqueda = true;
      this._medicoService.buscarMedicos(termino)
      .subscribe((medicos: any) => {
          this.medicos = medicos;
          this.cargando = false;
        });
    }

  }

  borrarMedico(medico: Medico) {
    swal({
      title: 'Esta seguro?',
      text: 'Esta a punto de borrar a ' + medico.nombre,
      icon: 'warning',
      buttons: [true, true],
      dangerMode: true,
    })
      .then((borrar) => {
        if (borrar) {
          this._medicoService.borrarMedico(medico._id)
            .subscribe((borrado) => {
              if ((this.desde + 1) > (this._medicoService.totalMedicos - 1)) {
                this.cambiarDesde(-5);
              } else {
                this.cargarMedicos();
              }
            });
        }
      });
  }

}
