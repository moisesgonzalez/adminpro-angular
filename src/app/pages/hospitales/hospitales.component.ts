import { Component, OnInit } from '@angular/core';
import { HospitalService, ModalUploadService, UsuarioService } from '../../services/service.index';
import { Hospital } from '../../models/hospital.model';

// import swal from 'sweetalert';

@Component({
  selector: 'app-hospitales',
  templateUrl: './hospitales.component.html',
  styles: []
})
export class HospitalesComponent implements OnInit {

  hospitales: Hospital[] = [];
  desde: number = 0;

  totalRegistros: number = 0;
  cargando: boolean = false;
  realizoBusqueda: boolean = false;

  constructor(
    public _hospitalService: HospitalService,
    public _modalUploadService: ModalUploadService,
    public _usuarioService: UsuarioService
  ) { }

  ngOnInit() {
    this.cargarHospitales();
    this._modalUploadService.notificacion.subscribe(resp => this.cargarHospitales());
  }

  mostrarModal(id: string) {
    this._modalUploadService.mostrarModal('hospitales', id);
  }

  cargarHospitales() {
    this.cargando = true;
    this._hospitalService.cargarHospitales(this.desde).
      subscribe((resp: any) => {
        this.cargando = false;
        this.totalRegistros = resp.total;
        this.hospitales = resp.hospitales;
      });
  }

  cambiarDesde(valor: number) {
    let desde = this.desde + valor;

    if (desde >= this.totalRegistros || desde < 0) {
      return;
    }

    this.desde += valor;
    this.cargarHospitales();
  }

  buscarHospitales(termino: string) {

    if (termino.length <= 0 && this.realizoBusqueda) {
      this.cargarHospitales();
      this.realizoBusqueda = false;
      return;
    }

    if (termino.length >= 3) {
      this.cargando = true;
      this.realizoBusqueda = true;
      this._hospitalService.buscarHospital(termino)
        .subscribe((hospitales: Hospital[]) => {
          this.hospitales = hospitales;
          this.cargando = false;
        });
    }

    return;
  }

  borrarHospital(hospital: Hospital) {

    swal({
      title: 'Esta seguro?',
      text: 'Esta a punto de borrar a ' + hospital.nombre,
      icon: 'warning',
      buttons: [true, true],
      dangerMode: true,
    })
    .then((borrar) => {
      if (borrar) {
        this._hospitalService.borrarHospital(hospital._id)
          .subscribe((borrado) => {
            if ((this.desde + 1) > (this.totalRegistros - 1)) {
              this.cambiarDesde(-5);
            } else {
              this.cargarHospitales();
            }
          });
      }
    });

  }

  crearHospital() {
    swal({
      title: 'Crear Hospital',
      text: 'Ingrese nombre del hospital',
      icon: 'info',
      content: {
        element: 'input',
        attributes: {
          type: 'text',
        },
      },
      buttons: [true, true],
      dangerMode: true
    })
    .then((nombre: string) => {
      if (!nombre || nombre.length === 0) {
        return;
      }

      return this._hospitalService.crearHospital(nombre).subscribe(hospital => {
        swal({
          title: 'Hospital creado',
          text: hospital.nombre,
          icon: 'success',
        })
        .then((ok) => {
          this.cargarHospitales();
        });
      },
      (err) => {
        swal('Ocurrio un error!', 'No se pudo crear el hospital', 'error');
      });

    });
  }

  guardarHospital(hospital: Hospital) {
    this._hospitalService.actualizarHospital(hospital).subscribe();
  }

}
