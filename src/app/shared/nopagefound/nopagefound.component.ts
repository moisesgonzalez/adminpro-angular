import { Component, OnInit } from '@angular/core';

declare function init_plugins();

@Component({
  selector: 'app-nopagefound',
  templateUrl: './nopagefound.component.html',
  styles: []
})
export class NopagefoundComponent implements OnInit {

  anio: number;

  constructor() { }

  ngOnInit() {
    init_plugins();
    this.anio = new Date().getFullYear();
  }

}
