import { Component, OnInit, ViewChild, ElementRef  } from '@angular/core';
import { SubirArchivoService, ModalUploadService, UsuarioService } from '../../services/service.index';

@Component({
  selector: 'app-modal-upload',
  templateUrl: './modal-upload.component.html',
  styles: []
})
export class ModalUploadComponent implements OnInit {
  @ViewChild('inputFile') myInputFile: ElementRef;

  imagenSubir: File;
  imagenTemp: string;

  constructor(
    public _subirArchivoService: SubirArchivoService,
    public _modalUploadService: ModalUploadService,
    public _usuarioService: UsuarioService
  ) { }

  ngOnInit() {
  }

  cerrarModal() {
    this.imagenSubir = null;
    this.imagenTemp = null;
    this.myInputFile.nativeElement.value = '';
    this._modalUploadService.ocultarModal();
  }

  seleccionImage(archivo: File) {

    if (!archivo) {
      this.imagenSubir = null;
      return;
    }

    if (archivo.type.indexOf('image') < 0) {
      swal('Sólo imágenes', 'El archivo seleccionado no es una imagen', 'error');
      this.imagenSubir = null;
      return;
    }

    this.imagenSubir = archivo;

    let reader = new FileReader();
    reader.readAsDataURL(archivo);

    reader.onloadend = () => { this.imagenTemp = reader.result };

  }

  subirImagen() {
    this._subirArchivoService.subirArchivo(
      this.imagenSubir,
      this._modalUploadService.tipo,
      this._modalUploadService.id
    )
    .then( (resp: any) => {

      if (this._modalUploadService.tipo === 'usuarios' && this._modalUploadService.id === this._usuarioService.usuario._id) {
        this._usuarioService.usuario.img = resp.usuario.img;
        this._usuarioService.guardarStorage(this._modalUploadService.id, this._usuarioService.token, resp.usuario, resp.usuario.menu);
      }

      this._modalUploadService.notificacion.emit(resp);
      this.cerrarModal();
      // swal( 'Imagen actualizada', '', 'success' );
    })
    .catch( err => {
      console.log('Error en la carga...', err);
    });
  }

}
