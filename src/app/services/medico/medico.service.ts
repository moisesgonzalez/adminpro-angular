import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Router } from '@angular/router';

import { Hospital } from '../../models/hospital.model';
import { URL_SERVICIOS } from '../../config/config';

import 'rxjs/add/operator/map';

// import * as _swal from 'sweetalert';
// import { SweetAlert } from 'sweetalert/typings/core';

import { SubirArchivoService } from '../subir-archivo/subir-archivo.service';
import { UsuarioService } from '../usuario/usuario.service';
import { Medico } from '../../models/medico.model';

@Injectable()
export class MedicoService {

  totalMedicos: number = 0;

  constructor(
    public http: HttpClient,
    public router: Router,
    public _usuarioService: UsuarioService,
    public _subirArchivoService: SubirArchivoService
  ) { }

  cargarMedicos(desde: number = 0) {
    let url = URL_SERVICIOS + '/medico?desde=' + desde;

    return this.http.get(url).map((resp: any) => {
      this.totalMedicos = resp.total;
      return resp.medicos;
    });
  }

  cargarMedico(id: string) {
    let url = URL_SERVICIOS + '/medico/' + id;
    return this.http.get(url).map((resp: any) => resp.medico);
  }

  buscarMedicos( termino: string ) {
    let url = URL_SERVICIOS + '/busqueda/coleccion/medicos/' + termino;
    return this.http.get(url).map((resp: any) => resp.medicos);
  }

  borrarMedico(id: string) {
    let url = URL_SERVICIOS + '/medico/' + id;
    url += '?token=' + this._usuarioService.token;

    return this.http.delete(url).map((resp: any) => {
      swal({
        title: 'Médico borrado',
        text: 'El médico a sido eliminado correctamente',
        icon: 'success'
      }).then((ok) => {
        return resp;
      });
    });
  }

  guardarMedico(medico: Medico) {
    let url = URL_SERVICIOS + '/medico';

    if (medico._id) {

      // Actualizando
      url += '/' + medico._id;
      url += '?token=' + this._usuarioService.token;

      return this.http.put(url, medico).map((resp: any) => {
        swal('Médico actualizado', medico.nombre, 'success');
        return resp.medico;
      });

    } else {

      // Creando
      url += '?token=' + this._usuarioService.token;

      return this.http.post(url, medico).map((resp: any) => {
        swal('Médico creado', medico.nombre, 'success');
        return resp.medico;
      });

    }

  }

}
